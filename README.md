# Avatar Search

Uses VRC Client and EMM DB to search for avatars.

Suggestion: Use a new VRChat account.

## Scripts

### `yarn install`

Install the project.

### `yarn start`

Run app in dev mode. [http://localhost:3000](http://localhost:3000)

### `yarn test`

Run tests.

### `yarn build`

Compiles the app to the `build` folder.

### `yarn open`

Execute `yarn build` then open app in electron.

### `yarn eject`

Don't run this...
