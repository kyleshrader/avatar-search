import { useContext } from "react"
import { ApiContext } from "./provider"
import vrc from 'vrchat-client'
import Client from '../services/emmApi'

const useApiContext = () => {
    const [state, setState] = useContext(ApiContext)

    async function login(username, password) {
        let vrc = await getVRCApi(username, password)
        let emm = await getEMMApi(vrc._userId, username)
        setState({ vrc, emm })
    }

    async function getVRCApi(username, password) {
        let vrcApi
        await vrc.login(username, password).then(api => vrcApi = api)
        return vrcApi

    }

    async function getEMMApi(userid, username) {
        let emmApi = new Client()
        emmApi.login(userid, username)
        return emmApi
    }

    return {
        ...state,
        login
    }
}

export { useApiContext }