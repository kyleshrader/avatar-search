import { useState } from 'react'
import SearchResults from './SearchResults'

const Search = ({vrc, emm}) => {
    const [query, setQuery] = useState('')
    const [searchResults, setSearchResults] = useState()

    const submit = (e) => {
        e.preventDefault()
        emm.search(query).then(setSearchResults)
    }

    return (
        <div>
            <form onSubmit={submit}>
                <label htmlFor='search'>Search:</label>
                <input type='text' name='search' onChange={e => setQuery(e.target.value)} />
                <input type='submit' value='Search' />
            </form>
            <SearchResults results={searchResults} />
        </div>
    )
}

export default Search