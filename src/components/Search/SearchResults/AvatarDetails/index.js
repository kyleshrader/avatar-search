import {CopyToClipboard} from 'react-copy-to-clipboard'

const AvatarDetails = ({details}) => {

    const download = () => {
        window.location.href = details.avatar_asset_url
    }

    return (
        <div>
            <button onClick={download}>Download</button>
            <img src={details.avatar_thumbnail_image_url} width='60px' alt={details.avatar_name}/>
            <div>{decode(details.avatar_name)}</div>
            <CopyToClipboard text={`https://vrchat.com/home/user/${details.avatar_author_id}`}>
                <div>{decode(details.avatar_author_name)}</div>
            </CopyToClipboard>
            <div>{details.avatar_supported_platforms === 1 ? 'PC Only': 'Quest/PC'}</div>
            <br />
            <hr />
        </div>
    )
}

function decode(string) {
    return Buffer.from(string, 'base64').toString('utf-8');
}

export default AvatarDetails