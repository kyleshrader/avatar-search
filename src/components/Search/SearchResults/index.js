import { useEffect, useState } from "react"
import AvatarDetails from "./AvatarDetails"

const SearchResults = ({results}) => {
    const [searchResults, setSearchResults] = useState([])

    useEffect(() => {
        if(results) {
            setSearchResults(results.map(result => (
                <AvatarDetails key={result.avatar_id} details={result} />
            )))
        }
    }, [results])

    return (
        <div>
            {searchResults}
        </div>
    )
}

export default SearchResults