import { useState } from 'react'

const Login = ({login, directLogin}) => {
    const [username, setUsername] = useState('')
    const [password, setPassword] = useState('')
    const [hasUserId, setHasUserId] = useState(false)
    const [userId, setUserId] = useState('')

    const submit = (e) => {
        e.preventDefault()
        hasUserId ? directLogin(userId, username) : login(username, password)
    }

    return (
        <div>
            <label htmlFor='hasUserId'>Have your UserId?</label>
            <input type='checkbox' name='hasUserId' onChange={e => setHasUserId(e.target.checked)} />
            <form onSubmit={submit}>
                <label htmlFor='username'>Username:</label>
                <input type='text' name='username' onChange={e => setUsername(e.target.value)} />
                { hasUserId 
                    ? <div>
                        <label htmlFor='userid'>UserId:</label>
                        <input type='text' name='userid' onChange={e => setUserId(e.target.value)} />
                    </div>
                    : <div>
                        <label htmlFor='password'>Password:</label>
                        <input type='password' name='password' onChange={e => setPassword(e.target.value)} />
                    </div>
                }
                <input type='submit' value='Login' />
            </form>
        </div>
    )
}

export default Login