import React, { useEffect, useState } from 'react'
import './style.css'
import Login from '../Login'
import Client from '../../services/emmApi'
import vrc from 'vrchat-client'
import Search from '../Search'

function App() {
  const [vrcApi, setVrcApi] = useState()
  const [emmApi, setEmmApi] = useState(new Client())
  const [loggedIn, setLoggedIn] = useState(false)

  const login = (username, password) => {
    vrc.login(username, password).then( api => {
      setVrcApi(api)
      emmApi.login(api._userId, username).then( () => 
        setLoggedIn(true)
      )
    })
  }

  const directLogin = (userid, username) => {
    emmApi.login(userid, username).then( () => 
      setLoggedIn(true)
    )
  }

  return (
    <div className="App">
      { loggedIn
        ? <Search vrc={vrcApi} emm={emmApi} />
        : <Login login={login} directLogin={directLogin} />
      }
    </div>
  )
}

export default App
