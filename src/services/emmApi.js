import Axios from 'axios'

export default class Client {
    constructor() {
        this.http = Axios.create({
            baseURL: 'https://thetrueyoshifan.com:3000'
        })
    }

    async login(userid, name) {
        let response = await this.http.post('/api/authentication/login', {
            username: userid,
            password: userid,
            name: name,
            loginKey: 1
        })

        this.token = response.data.token
    }

    async search(query) {
        let response = await this.http.post('/api/avatar/search', { query }, {
            headers: {
                Authorization: `Bearer ${this.token}`
            }
        })
        return response.data
    }
}
